import sys
from db import save_output
from services import fetch_hotels

# receives a comprehensive hotel_list, and a dictionary of legacy prices.
def get_prices(hotel_list, legacy_trimmed):
    legacy_id = legacy_trimmed.keys()
    curated_list = []
    for hotel in hotel_list:
        if hotel['id'] in legacy_id:
            try:
                tmp_price =  hotel['price']
                hotel['price'] = {'snaptravel': tmp_price, 'legacy': legacy_trimmed[int(hotel['id'])]}
                curated_list.append(hotel)
            except Exception as  e:
                print(e)
    return curated_list


# main script
def main(argv):
    try:
        city_input = str(argv[0])
        checkin_input = str(argv[1])
        checkout_input = str(argv[2])
    except:
        print('failed on arguments')

    hotel_array, legacy_array = fetch_hotels(city_input, checkin_input, checkout_input)


    # dict of legacy prices
    legacy_id_price = dict(zip(map(lambda x: int(x["id"]), legacy_array),map(lambda x: float(x["price"]), legacy_array)))

    # merge prices and save
    
    save_output(get_prices(hotel_array, legacy_id_price))



if __name__ == "__main__":
    #           ANY VALIDATION
    # try:
    #     opts, args = getopt.getopt(argv,"abc")
    # except getopt.GetoptError:
    #     print ('script.py -c <city_string> -chi <checkin-string> -cho <checkin-string> ')
    #     sys.exit(2)
    # for opt, arg in opts:
    #     if opt == '-h':
    #         print ('script.py -c <city_string> -chi <checkin-string> -cho <checkin-string> ')
    #         sys.exit()
    #     elif opt in "-c":
    #         inputfile = arg
    #     elif opt in "-o":
    #         outputfile = arg

    if len(sys.argv) == 4:
        main(sys.argv[1:])
    else:
        print('not enough arguments')
