import json

def save_output(hotel_list):
    with open("response_file.json", "w") as write_file:
        for h in hotel_list:
            json.dump(h, write_file)