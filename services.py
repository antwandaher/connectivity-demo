import requests
import asyncio
from concurrent.futures import ThreadPoolExecutor
import xml.etree.ElementTree as ET
from functools import partial
import json

def fetch_hotels(city_input, checkin_input, checkout_input):
    loop = asyncio.get_event_loop()
    #parallel async calls
    resp_lis = loop.run_until_complete(fetch_requests(city_input, checkin_input, checkout_input))

    # split the request responses
    hotel_array = json.loads(resp_lis[0].text)["hotels"]
    legacy_array =  xml_to_json(resp_lis[1].text)

    return hotel_array, legacy_array


# async parallel https requests using asyncio
async def fetch_requests(city, checkin, checkout):
    hotel_link = 'https://experimentation.getsnaptravel.com/interview/hotels'
    legacy_link = 'https://experimentation.getsnaptravel.com/interview/legacy_hotels'

    hotel_json = {  
        'city' : city, 
        'checkin' : checkin, 
        'checkout' : checkout, 
        'provider' : 'snaptravel'
    }

    legacy_data = """
    <?xml version="1.0" encoding="UTF-8"?>
    <root>
      <checkin>%s</checkin>
      <checkout>%s</checkout>
      <city>%s</city>
      <provider>snaptravel</provider>
    </root>
    """.format(city, checkin, checkout)

    legacy_headers = {'Content-Type': 'application/xml'}

    hotel_req = partial(requests.post,url=hotel_link, json=hotel_json)
    legacy_req = partial(requests.post,url=legacy_link, data=legacy_data, headers=legacy_headers)

    executor = ThreadPoolExecutor(max_workers=5)
    loop = asyncio.get_event_loop()

    futures = [
        loop.run_in_executor(executor, hotel_req),
        loop.run_in_executor(executor, legacy_req)
    ]
    resp_ls =[]
    for response in await asyncio.gather(*futures):
        resp_ls.append(response)

    return resp_ls


# transform xml structure to json. expected skeleton is outlined first.
def xml_to_json(xml):
    root = ET.fromstring(xml)
    array_json = []
    for hotel in root.findall('element'):
        _json = {
            "id": None,
            "hotel_name": None, 
            "num_reviews": None, 
            "address": None, 
            "num_stars": None, 
            "amenities": None, 
            "image_url": None, 
            "price": None, 
        }

        for info_key in hotel:
            try:
                _json[info_key.tag] = info_key.text
            except Exception as e:
                print(e)
        array_json.append(_json)
    return array_json
